#include <Adafruit_NeoPixel.h>
#ifdef __AVR__
#include <avr/power.h> // Required for 16 MHz Adafruit Trinket
#endif
#include <FastLED.h>

#define PIN_NEO_PIXEL  8  // Arduino pin that connects to NeoPixel
#define NUM_PIXELS     60  // The number of LEDs (pixels) on NeoPixel
#define PIN 7
#define NUM_LED 42

Adafruit_NeoPixel NeoPixel(NUM_PIXELS, PIN_NEO_PIXEL, NEO_GRB + NEO_KHZ800);
Adafruit_NeoPixel NeoPixel1(NUM_LED, PIN, NEO_GRB + NEO_KHZ800);


void setup() {
  NeoPixel.begin(); // INITIALIZE NeoPixel strip object (REQUIRED)
  NeoPixel1.begin();
}

void loop() {
  NeoPixel.clear(); // set all pixel colors to 'off'. It only takes effect if pixels.show() is called
  NeoPixel1.clear();
        // turn pixels to green one by one with delay between each pixel
 
  for(int pixel = 0; pixel < NUM_PIXELS; pixel++) { // for each pixel
    NeoPixel.setPixelColor(pixel, NeoPixel.Color(0, 0, 255)); // it only takes effect if pixels.show() is called
    // pause between each pixel

//for(int pixel = 0; pixel < NUM_LED; pixel++) { // for each pixel
  //  NeoPixel1.setPixelColor(pixel, NeoPixel1.Color(255, 0, 0)); // it only takes effect if pixels.show() is called
   //delay(10); // pause between each pixel   
  }
    NeoPixel.show();   // send the updated pixel colors to the NeoPixel hardware.
    NeoPixel1.show();
  
  
  }

